import requests
import json
from xml.dom import minidom
from enum import Enum
import igb_trackhub.settings as settings

class HTTPErrors(Enum):
    NOT_FOUND = "404 error"

class CreateResources:

    def __init__(self):
        self.igb_species_rows = []
        self.igb_synonyms_rows = []
        self.quickload_contents = []
        self.unique_extensions = []
        resource_url = settings.IGB_REPOSITORY.strip("/") + '/raw/' + settings.IGB_BRANCH + "/core/synonym-lookup/src/main/resources/"
        species = requests.get(resource_url + "species.txt")
        if species:
            self.igb_species_rows = species.text.split("\n")
        synonyms = requests.get(resource_url + "synonyms.txt")
        if synonyms:
            self.igb_synonyms_rows = synonyms.text.split("\n")
        contents = requests.get(settings.QUICKLOAD_CONTENTS_TXT)
        if contents:
            self.quickload_contents = contents.text.split("\n")

    def create_contents_txt(self, hub_link):
        """
           Obtain a contents.txt string to allow IGB to determine the genomes available in the data source. Genome names
           are UCSC-derived. Column one holds genome names and column two holds genome descriptions. In cases where a
           UCSC genome has a matching IGB synonym, the the default IGB genome description is used. Otherwise, if the hub
           provides a genome description, that is used; if it isn't, an external API is used to obtain it.


           :param str hub_link: The URL of a UCSC TrackHub.
           :return: The string representation of contents.txt.
           :rtype: str
        """
        url = "https://api.genome.ucsc.edu/list/hubGenomes?hubUrl=" + hub_link
        res = requests.get(url)
        # Each line contains a tab-delimited `genome name` and `genome description`.
        result = ""
        if res:
            res_json = json.loads(res.text)
            genomes = res_json["genomes"]
            for genome in genomes:
                # Attempt to obtain the genome description from the default quickload contents.txt
                description = self.get_quickload_genome_description(genome)
                if not description:
                    # Get genome description from UCSC API
                    description = str(genomes[genome]["description"])
                if not description:
                    # Default the genome description to the genome name
                    description = genome
                result += str(genome) + "\t" + str(description) + "\n"
        else:
            result = HTTPErrors.NOT_FOUND.value
        return result

    def create_species_txt(self, hub_link):
        """
           Obtain a species.txt string to allow IGB to associate particular genomes with their respective species.
           Genomes for which this information cannot be found are loaded in IGB under the species 'Trackhub misc'.

           :param str hub_link: The URL of a UCSC TrackHub.
           :return: The string representation of species.txt.
           :rtype: str
        """
        url = "https://api.genome.ucsc.edu/list/hubGenomes?hubUrl=" + hub_link
        res = requests.get(url)
        if res:
            # Create a dictionary mapping UCSC organism names to UCSC genome names
            res_json = json.loads(res.text)
            organisms_genomes = {}
            for genome in res_json["genomes"].keys():
                organism = None
                # Attempt to determine IGB organism using UCSC genome
                igb_genome = self.get_default_igb_genome_name(genome)
                if igb_genome:
                    reference_genome = "_".join(igb_genome.split("_")[:2]).lower()
                    for species_row in self.igb_species_rows:
                        if reference_genome in species_row.lower():
                            organism = species_row.split("\t")[0]
                            break
                if not organism:
                    # Fall back to UCSC organism name
                    organism = res_json["genomes"][genome]["organism"]
                    # Attempt to determine IGB organism using UCSC organism
                    for species_row in self.igb_species_rows:
                        if organism.lower() in species_row.lower():
                            organism = species_row.split("\t")[0]
                if not organism:
                    # Default to generic genome grouping
                    organism = "Trackhub misc"
                if organisms_genomes.get(organism) is None:
                    organisms_genomes[organism] = []
                organisms_genomes[organism] += [genome]

            # Generate species.txt string
            result = ""
            for organism in organisms_genomes:
                for genome in organisms_genomes[organism]:
                    result += organism.capitalize() + "\t" + organism.capitalize() + "\t" + genome + "\n"

            return result

        else:
            return HTTPErrors.NOT_FOUND.value

    def create_genome_txt(self, hub_link, genome):
        """
           Obtain a genomes.txt string to allow loading chromosome information for a particular genome in IGB.

           :param str hub_link: The URL of a UCSC TrackHub.
           :param str genome: The genome name IGB uses to detail requests for genomes.txt and annots.xml. The genome
            name is based on UCSC trackhub information, however, it may coincide with an official IGB name. IGB
            genome names are described in the first column of the default synonyms.txt.
           :return: The string representation of genomes.txt.
           :rtype: str
        """
        # If a genome.txt is requested for a genome already in the default IGB synonyms.txt - and therefore one loaded
        # by a default IGB data source, which contains its own genome.txt - don't return anything, to prevent
        # chromosome info duplication. This assumes that the default synonyms.txt is always loaded in IGB, regardless of
        # whether or not these data sources are enabled.
        if any(map(lambda row: genome in row, self.igb_synonyms_rows)):
            return ""

        # Build and return genomes.txt only if a custom reference genomic sequence (2bit URL) is provided. For cases in
        # which it is not, it is assumed that the UCSC genome name matches an IGB synonym. If it doesn't, as there is no
        # sense in loading a trackhub without a reference sequence, an empty response is returned.
        url = "https://api.genome.ucsc.edu/list/chromosomes?hubUrl=" + hub_link + ";genome={};track=".format(genome)
        res = requests.get(url)
        _, two_bit_file = self.get_genome_db_loc_2bit(hub_link, genome)
        result = ""
        if res and two_bit_file:
            chromosomes = json.loads(res.text)["chromosomes"]

            for x in chromosomes:
                result += str(x) + "\t" + str(chromosomes[x]) + "\n"

        return result

    def create_annots_xml(self, hub_link, genome):
        """
           Obtain an annots.xml string to allow IGB to determine the data and its locations for a particular genome.

           :param str hub_link: The URL of a UCSC TrackHub.
           :param str genome: The genome name IGB uses to detail requests for genomes.txt and annots.xml. The genome
            name is based on UCSC trackhub information, however, it may coincide with an official IGB name. IGB
            genome names are described in the first column of the default synonyms.txt.
           :return: The string representation of annots.xml.
           :rtype: str
        """
        genome_db_2bit = self.get_genome_db_loc_2bit(hub_link, genome)
        if genome_db_2bit is None:
            return HTTPErrors.NOT_FOUND.value
        track_db_loc, two_bit_file = genome_db_2bit
        full_path = '/'.join(track_db_loc.split('/')[:-1]) + "/"
        res = requests.get(track_db_loc)
        if res.status_code == 200:
            con = res.text.split(" ")
            if con[0] == "include":
                url = full_path + con[1]
                url = url.strip('\n')
                res = requests.get(url)
        if res.status_code == 200:
            content = res.text.split("\n")
            # create file
            root = minidom.Document()
            # creat root element
            xml = root.createElement('files')
            root.appendChild(xml)

            if two_bit_file:
                filesChild = root.createElement('file')
                filesChild.setAttribute('name', two_bit_file)
                filesChild.setAttribute('reference', "true")
                xml.appendChild(filesChild)
            trackName = bigDataUrl = type = shortLabel = description = html_url = foreground = None
            trackhub_extensions = []
            for line in content:
                line = line.strip()
                label_content = line.split(" ", maxsplit=1)
                # extract fields and values
                if len(label_content) == 2:
                    label, content = label_content
                    if label == "track":
                        trackName = content
                    elif label == "shortLabel":
                        shortLabel = content
                    elif label == "bigDataUrl":
                        bigDataUrl = content
                        bigDataUrl = bigDataUrl if "http" in bigDataUrl else full_path + bigDataUrl
                        ext = bigDataUrl.split("/")[-1].split(".")[-1]
                        if ext not in self.unique_extensions:
                            self.unique_extensions.append(ext)
                        if ext not in trackhub_extensions:
                            trackhub_extensions.append(ext)
                    elif label == "type":
                        type = content
                    elif label == "longLabel":
                        description = content
                    elif label == "html":
                        html_url = "/".join(track_db_loc.split("/")[:-1]) + "/" + content
                    elif label == "color":
                        rgb = list(map(lambda color_string: int(color_string), content.split(",")))
                        foreground = CreateResources.rgb_to_hex(tuple(rgb))
                # add data to xml file at the end of each block in trackDb.txt
                if line == "" and bigDataUrl:
                    filesChild = root.createElement('file')
                    filesChild.setAttribute('name', bigDataUrl)
                    filesChild.setAttribute('title', (trackName + "/" + type + "/" + shortLabel).replace(" ", "_"))
                    if html_url:
                        filesChild.setAttribute('url', html_url)
                    if description:
                        filesChild.setAttribute('description', description)
                    if foreground:
                        filesChild.setAttribute('foreground', foreground)
                    xml.appendChild(filesChild)
                    trackName = bigDataUrl = type = shortLabel = description = html_url = foreground = None

            print(hub_link + " extensions:\n" + str(trackhub_extensions) + "\n")
            xml_str = root.toprettyxml(indent="\t")

            return xml_str
        return HTTPErrors.NOT_FOUND.value

    @staticmethod
    def rgb_to_hex(rgb):
        return '%02x%02x%02x' % rgb

    def get_default_igb_genome_name(self, genome):
        """
            Attempt to determine the default IGB genome version name using the default synonyms.txt file.

            :param str genome: A UCSC genome name
            :return: The default IGB genome version name associated with the input genome, if available.
            :rtype: str | None
        """
        for synonyms_row in self.igb_synonyms_rows:
            if genome in synonyms_row:
                return synonyms_row.split("\t")[0]

    def get_quickload_genome_description(self, genome):
        """
            Attempt to determine the genome description for the default IGB genome version name using the default
            quickload contents.txt file.

            :param str genome: A UCSC genome name
            :return: The default quickload genome description associated with the input genome, if available.
            :rtype: str | None
        """
        igb_genome = self.get_default_igb_genome_name(genome)
        if not igb_genome:
            return
        for line in self.quickload_contents:
            if line != "":
                ct_genome, ct_genome_description = line.split("\t")
                if igb_genome.lower() == ct_genome.lower():
                    return ct_genome_description

    @staticmethod
    def get_genome_db_loc_2bit(hub_link, genome):
        """
           Obtain a database URL and a 2bit file URL for a UCSC genome.

           :param str hub_link: The URL of a UCSC trackhub.
           :param str genome: The genome name IGB uses to detail requests for genomes.txt and annots.xml. The genome
            name is based on UCSC trackhub information, however, it may coincide with an official IGB name. IGB
            genome names are described in the first column of the default synonyms.txt.
           :return: A tuple of `database URL` and optionally empty `2bit URL` or None.
           :rtype: (str, str) | None
        """
        url = "https://api.genome.ucsc.edu/list/hubGenomes?hubUrl=" + hub_link
        res = requests.get(url)
        if res:
            res_json = json.loads(res.text)
            res_genome = res_json["genomes"][genome]
            return res_genome['trackDbFile'], res_genome['twoBitPath']
        else:
            return None

    def get_igb_genome_versions(self, genomes):
        """
            Return the IGB equivalent for the input UCSC genome names, if found. Otherwise return None.

            :param str genomes: An array of objects containing UCSC organism names as keys and associated UCSC genome
            names as values.
            :return: A modified version of the input array with UCSC genome names replaced with either the default IGB
            equivalent or 'None'.
            :rtype: str
        """
        modified_arr = []
        for obj in genomes:
            modified_obj = obj
            for organism in obj.keys():
                igb_genomes = []
                for genome in obj[organism]:
                    default_igb_genome_name = self.get_default_igb_genome_name(genome)
                    if default_igb_genome_name:
                        igb_genomes.append(default_igb_genome_name)
                    else:
                        igb_genomes.append('None')
                modified_obj[organism] = igb_genomes
            modified_arr.append(modified_obj)
        return json.dumps(modified_arr)