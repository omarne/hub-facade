from django import forms


class TrackhubForm(forms.Form):
    th_link = forms.CharField(label='Your URL', max_length=150)
