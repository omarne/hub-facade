# Track Hub Facade API

---

## Running

### Create and enter a virtual environment for python3

`virtualenv -p python3 venv`

`. venv/bin/activate`

### Install dependencies

`pip install -r requirements.txt`

### Start the server

`cd igb_trackhub/`

`python manage.py runserver`

---

### Configuration

To run locally, you must configure Django to allow connections from localhost. Modify `igb_trackhub/igb_trackhub/settings.py` to include the change below:
`ALLOWED_HOSTS = ['127.0.0.1', 'localhost']`

### About

Due to the nature of IGB's quickload system, which loads data sources under the assumption that their associated metadata exist in a particular directory structure, this API was designed to have a particular trackhub data source have an identical resource path of `https://$(DOMAIN)/api/` combined with a `hubUrl` query parameter across endpoints, to resemble a shared directory. The `filePath` query parameter, which IGB populates as a path to a particular metadata file, differentiates the endpoints. As such, each endpoint is intended to be used for retrieving a particular metadata file.

### Endpoints

#### contents.txt:
##### Type: GET

- `https://$(DOMAIN)/api/?hubUrl=$(HUB_URL)&filePath=/contents.txt`
- Returns a list of available genome versions for a given track hub
- e.g. `https://localhost/api/?hubUrl=http://ftp.ebi.ac.uk/pub/databases/blueprint/releases/current_release/homo_sapiens/hub/hub.txt&filePath=/contents.txt`

#### genome.txt:
##### Type: GET

- `https://$(DOMAIN)/api/?hubUrl=$(HUB_URL)&filePath=/$(GENOME_VERSION)/genome.txt`
- Returns a list of chromosome names and their sequence locations for a given genome version and track hub
- e.g. `https://localhost/api/?hubUrl=http://ftp.ebi.ac.uk/pub/databases/blueprint/releases/current_release/homo_sapiens/hub/hub.txt&filePath=/hg38/genome.txt`

#### annots.txt:
##### Type: GET

- `https://$(DOMAIN)/api/?hubUrl=$(HUB_URL)&filePath=/$(GENOME_VERSION)/annots.xml`
- Returns the data files available for loading in IGB for a given genome version and track hub
- e.g. `https://localhost/api/?hubUrl=http://ftp.ebi.ac.uk/pub/databases/blueprint/releases/current_release/homo_sapiens/hub/hub.txt&filePath=/hg38/annots.xml`

#### igbGenomeVersion:
##### Type: POST

- `https://$(DOMAIN)/api/igbGenomeVersions`
  - Request body: { `ORGANISM_NAME`: [ `GENOME1`, `GENOME2`, `...` ] }
- Returns the IGB equivalent for the UCSC genome names, if found.
- e.g. `http://127.0.0.1:8000/api/igbGenomeVersions`
  - Request body: 
    - {
        "ucscGenomes": [
            {
            "Dromaius novaehollandiae": [
                "GCF_003342905.1",
                "GCA_016128335.1"
            ]
            }
        ]
    }

---

## Testing

### Watch requests made by IGB

Monitor all outgoing HTTP traffic on a machine (assumes your active outbound interface is en0):

`tcpdump -A -i en0 'tcp[((tcp[12:1] & 0xf0) >> 2):4] = 0x47455420'`

### List unique data file extensions

- `https://$(DOMAIN)/api/?filePath=/data_extensions.log`
  - The returned set is updated every time the annots.txt endpoint is hit, from when the server was started.

---

## Deploying

See the `ansible-playbooks` folder for the configurations describing deployment.
